[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)
![MIT License](https://img.shields.io/static/v1?label=License&message=MIT&color=RED)


# CiscoL2Sec Python Network Security Automation Script

This Is The Network Security Automation Script For Spoofing Attack Prevention In Cisco Layer 2 Switch

# Developer: AmirHosein Tangsiri Nezhad

## installation:
➜  ~ git clone https://github.com/AmirHoseinTangsiriNET/CiscoL2Sec/

➜  ~ cd CiscoL2Sec

➜  ~ python Cisco2Sec.py

## Attack That This Script Can Prevent:

* DHCP Spoofing Attack

* DHCP Starvation Attack 

* Arp Spoofing Attack

* Arp Cache Poisoning Attack

## Prerequisite
* Python 
* sys
* telnetlib
* colorama
* time